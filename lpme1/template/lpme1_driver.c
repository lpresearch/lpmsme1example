/***********************************************************************
**
**
** Copyright (C) 2013 LP-Research
** All rights reserved.
** Contact: LP-Research (info@lp-research.com)
***********************************************************************/

/**********************************************************************
  * Low level driver for lpms-me1 motion sensor
**********************************************************************/

#include "lpme1_driver.h"

/**
  *@brief: Read register value from lpms-me1 motion sensor
  *@para: regaddr, register address to be read
  *@para: buf, Pointer to uint8_t variable use to save read value
  *@ret: Status, return LPME1_OK if read success otherwise return LPME1_ERROR
  */
lpme1_status_t lpme1_read_reg(uint8_t regaddr,uint8_t *buf)
{
	//To be completed by user depens on platform
}
/**
  *@brief: Write value  to lpms-me1 motion sensor register
  *@para: regaddr, register address to be write
  *@para: buf, Pointer to uint8_t variable saved the data to be write
  *@ret: Status, return LPME1_OK if read success otherwise return LPME1_ERROR
  */
lpme1_status_t lpme1_write_reg(uint8_t regaddr,uint8_t *buf)
{
	//To be completed by user depens on platform
}
/**
  *@brief: Read a set of registers from lpms-me1 motion sensor
  *@para: regaddr, register start address to be read
  *@para: buf, Pointer to uint8_t array use to save read datas
  *@para: len, data length to be read
  *@ret: Status, return LPME1_OK if read success otherwise return LPME1_ERROR
  */
lpme1_status_t lpme1_read_buffer(uint8_t regaddr,uint8_t *buf,uint8_t len)
{
	//To be completed by user depens on platform
}