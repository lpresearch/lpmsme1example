/***********************************************************************
**
**
** Copyright (C) 2013 LP-Research
** All rights reserved.
** Contact: LP-Research (info@lp-research.com)
***********************************************************************/

#ifndef __LPME1_DRIVER_H
#define __LPME1_DRIVER_H

#include "lpme1.h"

lpme1_status_t lpme1_read_reg(uint8_t regaddr,uint8_t *buf);
lpme1_status_t lpme1_write_reg(uint8_t regaddr,uint8_t *buf);
lpme1_status_t lpme1_read_buffer(uint8_t regaddr,uint8_t *buf,uint8_t len);void lpme1_test(void);

#endif/*__LPME1_H*/
