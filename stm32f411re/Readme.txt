 
    An example project for lpms-me1 motion sensor
    Show user how to user lpme1 driver library
    
    Based on stm32f411re Nucleo , USE STM32 HAL DRIVER
    
Function:
    Read all data from lpms-me1,then transmit some data out through UART communication port.
    
Files：
/Drivers/: STM32 HAL driver
/Inc/: Include files
/Src/: Source files
/EWARM/: Embedded workbench ,IAR project
/MDK-ARM/: Keil-MDK project
/stm32f411re.ioc/: STM32CubeMX project