    
    Drvier and example for lpms-me1 motion sensor
    
Files:
    /lpme1/ : Drvier for lpms-me1 motion sensor
    /stm32f411re/: Example project based on stm32,
                   test on STM32F411RE NUCLEO and can be easily tailored to any other stm32 devices.
    
    
   
    
    Copyright (C) 2013 LP-Research
    All rights reserved.
    Contact: LP-Research (info@lp-research.com)